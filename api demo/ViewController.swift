//
//  ViewController.swift
//  api demo
//
//  Created by RP-14 on 13/05/22.
//

import UIKit
import Alamofire
import Kingfisher


var baseURL = "https://dummyapi.io/data/v1/user?limit=10"

class ViewController: UIViewController {
    @IBOutlet weak var myTable: UITableView!
    var model = [userListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCall()
    }
    func apiCall() {
        //api call with alamofire
     
        AF.request(baseURL,method: .get,headers: ["app-id" : "627e1873fd302d05b417381a"]).responseJSON{ response in
//            print (response)
            switch response.result{
            
            case .success(let value):
               
                if let json = value as? [String:Any]
                
                {
                    if let jsondata = json["data"] as? [[String:Any]]
                    
                    {
//                        print ("working",jsondata)
                        for i in jsondata{
//                            print ("user details",i)
                            self.model.append(userListModel(fromData: i))
                        }
                        print (self.model.count)
                        self.myTable.reloadData()
                    }
                    
                }
                
                
                
                
                
                
            case .failure(let error):
                print("ERROR ------",error)
            }
            
        }
    }
    func apiCall2(){
        //api call with URL session
        let url = URL(string: "https://dummyapi.io/data/v1/user?limit=10")! //change the url
            
           //create the session object
           let session = URLSession.shared
                
           //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = ["app-id" : "627e1873fd302d05b417381a"]
      
           //create dataTask using the session object to send data to the server
           let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                    
               guard error == nil else {
                   return
               }
                    
               guard let data = data else {
                   return
               }
                    
              do {
                 //create json object from data
                 if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                 }
              } catch let error {
                print(error.localizedDescription)
              }
           })

           task.resume()

//        task.resume()

    }

}
extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTable.dequeueReusableCell(withIdentifier: "CellClass") as! CellClass
        cell.labelInCell.text = model[indexPath.row].firstName
        cell.imageInCell.kf.setImage(with: URL(string: model[indexPath.row].picture))
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}






class userListModel{
    var firstName : String!
    var id : String!
    var lastName : String!
    var picture : String!
    var title : String!
    init(fromData data: [String:Any]){
        self.firstName = data["firstName"] as? String
        self.id = data["id"]as! String
        self.lastName = data["lastName"] as! String
        self.picture = data["picture"] as! String
        self.title = data["title"] as! String
    }
}
