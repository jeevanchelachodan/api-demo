//
//  CellClass.swift
//  api demo
//
//  Created by RP-14 on 13/05/22.
//

import UIKit

class CellClass: UITableViewCell {

    @IBOutlet weak var labelInCell: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imageInCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
